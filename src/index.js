import React from 'react';
import ReactDOM from 'react-dom';
import Clock from 'react-clock';

class ShowDateTime extends React.Component {

  //setting states
  state = {
    date: new Date(),
    value: "",
  }

 
  componentDidMount() {
    setInterval(
      ()=>{
        let now = new Date()
        let value = ""
        if(now.getSeconds() % 3 == 0 ){
          value += "fuzz"
        }
        if(now.getSeconds() % 5 == 0 ){
          value += "buzz"
        }
        this.setState({date:now, value:value})
      },1000
    )
  }
 
  render() {
    return (
      <div>
        <p>Current time: {this.state.date.toString()}</p>
        <Clock
          value={this.state.date}
        />
        <h1>{this.state.value} : {this.state.date.getSeconds()}</h1>
      </div>
      
    );
  
}
}
ReactDOM.render(<ShowDateTime/>, document.getElementById('root'));